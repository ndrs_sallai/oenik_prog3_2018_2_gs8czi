﻿//-----------------------------------------------------------------------
// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Logic
{
    using System.Linq;

    using ShipDatabase.Data;
    using ShipDatabase.Repository;

    /// <summary>
    /// BusinessLogic class
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// Constructor
        /// </summary>
        /// <param name="repo">repo</param>
        public BusinessLogic(IRepository repo)
        {
            this.repository = repo;
        }

        /// <summary>
        /// Create ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="bedspace">bedspace</param>
        /// <param name="ownerSsn">ownerSsn</param>
        /// <returns>the new ship</returns>
        public ship CreateShip(string name, short? bedspace, string ownerSsn)
        {
            return this.repository.CreateShip(name, bedspace, ownerSsn);
        }

        /// <summary>
        /// List ships
        /// </summary>
        /// <returns>the ship list</returns>
        public IQueryable<ship> ListShips()
        {
            return this.repository.ListShips();
        }

        /// <summary>
        /// Update ship
        /// </summary>
        public void UpdateShip()
        {
            this.repository.UpdateShip();
        }

        /// <summary>
        /// Delete ship
        /// </summary>
        /// <param name="name">name</param>
        public void DeleteShip(string name)
        {
            this.repository.DeleteShip(name);
        }

        /// <summary>
        /// List owners
        /// </summary>
        /// <returns>the owner list</returns>
        public IQueryable<owner> ListOwners()
        {
            return this.repository.ListOwners();
        }

        /// <summary>
        /// List clubs
        /// </summary>
        /// <returns>the club list</returns>
        public IQueryable<club> ListClubs()
        {
            return this.repository.ListClubs();
        }

        /// <summary>
        /// Dummy method for test setup
        /// </summary>
        /// <param name="a">first number</param>
        /// <param name="b">second number</param>
        /// <returns>the sum of numbers</returns>
        public int Add(int a, int b)
        {
            return a + b;
        }

        /// <summary>
        /// Create owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        /// <param name="name">name</param>
        /// <returns>the new owner entity</returns>
        public owner CreateOwner(string ssn, string name)
        {
            return this.repository.CreateOwner(ssn, name);
        }

        /// <summary>
        /// Update owner
        /// </summary>
        public void UpdateOwner()
        {
            this.repository.UpdateOwner();
        }

        /// <summary>
        /// Delete owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        public void DeleteOwner(string ssn)
        {
            this.repository.DeleteOwner(ssn);
        }

        /// <summary>
        /// Create club
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <returns>the new club entity</returns>
        public club CreateClub(string name, string phoneNumber)
        {
            return this.repository.CreateClub(name, phoneNumber);
        }

        /// <summary>
        /// Update club
        /// </summary>
        public void UpdateClub()
        {
            this.repository.UpdateClub();
        }

        /// <summary>
        /// Delete club
        /// </summary>
        /// <param name="name">name</param>
        public void DeleteClub(string name)
        {
            this.repository.DeleteClub(name);
        }

        /// <summary>
        /// Returns a nicely formatted string representing the ship entity
        /// </summary>
        /// <param name="s">ship entity</param>
        /// <returns>the formatted output</returns>
        public string PresentShip(Data.ship s)
        {
            return $"{s.name} ({s.manufacturer_name}) stationed in {s.homeport_name}";
        }
    }
}

﻿//-----------------------------------------------------------------------
// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Logic
{
    using System.Linq;
    using ShipDatabase.Data;

    /// <summary>
    /// ILogic interface
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Create ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="bedspace">bedspace</param>
        /// <param name="ownerSsn">ownerSsn</param>
        /// <returns>the new ship entity</returns>
        ship CreateShip(string name, short? bedspace, string ownerSsn);

        /// <summary>
        /// List ships
        /// </summary>
        /// <returns>the ship list</returns>
        IQueryable<ship> ListShips();

        /// <summary>
        /// Update ship
        /// </summary>
        void UpdateShip();

        /// <summary>
        /// Delete ship by name
        /// </summary>
        /// <param name="name">name</param>
        void DeleteShip(string name);

        /// <summary>
        /// Create owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        /// <param name="name">name</param>
        /// <returns>the new owner entity</returns>
        owner CreateOwner(string ssn, string name);

        /// <summary>
        /// List owners
        /// </summary>
        /// <returns>the owner list</returns>
        IQueryable<owner> ListOwners();

        /// <summary>
        /// Update owner
        /// </summary>
        void UpdateOwner();

        /// <summary>
        /// Delete owner by ssn
        /// </summary>
        /// <param name="ssn">ssn</param>
        void DeleteOwner(string ssn);

        /// <summary>
        /// Create club
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <returns>the new club entity</returns>
        club CreateClub(string name, string phoneNumber);

        /// <summary>
        /// List clubs
        /// </summary>
        /// <returns>the club list</returns>
        IQueryable<club> ListClubs();

        /// <summary>
        /// Upadte club
        /// </summary>
        void UpdateClub();

        /// <summary>
        /// Delete club by name
        /// </summary>
        /// <param name="name">name</param>
        void DeleteClub(string name);

        /// <summary>
        /// Returns a nicely formatted string representing the ship entity
        /// </summary>
        /// <param name="s">ship entity</param>
        /// <returns>the formatted output</returns>
        string PresentShip(Data.ship s);
    }
}

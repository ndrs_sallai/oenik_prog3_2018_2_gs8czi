var class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row =
[
    [ "GetmembershipRows", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#ade4e2658802da66b0c6b4228df529d91", null ],
    [ "GetshipRows", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#ae4663d35a58d011bc3f88ced37991957", null ],
    [ "Isaddress_idNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a4b4663d581f67e0554b2935957b5574d", null ],
    [ "Isdate_of_birthNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#aed0849df8822fd4bdfb225b22bfccff6", null ],
    [ "Setaddress_idNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#aae08f9b7ab454726c3af03f1f229c480", null ],
    [ "Setdate_of_birthNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a885080f6e27b6bc9749623483618ef5d", null ],
    [ "address_id", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#aac3a7b3fcecd42ebd7e66dbc189d71bf", null ],
    [ "addressRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a4cd27858f8c1abe2c7eeed67ac639211", null ],
    [ "date_of_birth", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a047ba6f4ce513ec464545ef97af88551", null ],
    [ "name", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a16c7c2278e5bcf4600e935294f55db33", null ],
    [ "ssn", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html#a554760e3f5cf1688e39f13006a1f747e", null ]
];
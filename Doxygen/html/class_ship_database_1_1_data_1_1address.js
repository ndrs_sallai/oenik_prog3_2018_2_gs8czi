var class_ship_database_1_1_data_1_1address =
[
    [ "address", "class_ship_database_1_1_data_1_1address.html#a970bac8b4d1b2f4bf8f1e8ba50fb3bca", null ],
    [ "city", "class_ship_database_1_1_data_1_1address.html#aa92c65ac4e9a99ac0d193c4b0bd8e675", null ],
    [ "clubs", "class_ship_database_1_1_data_1_1address.html#a8b658270dc6a86b67da264b598e88e52", null ],
    [ "homeports", "class_ship_database_1_1_data_1_1address.html#a45f05b1f47295a8749b184f50e4bbf46", null ],
    [ "id", "class_ship_database_1_1_data_1_1address.html#a385fb6b62392b628f864b17d628e6afa", null ],
    [ "manufacturers", "class_ship_database_1_1_data_1_1address.html#aa928b6d9c56594bd384e75d5ea50766d", null ],
    [ "owners", "class_ship_database_1_1_data_1_1address.html#ab9f6a2f31c7977e7b7d627b33beb3e62", null ],
    [ "postal_code", "class_ship_database_1_1_data_1_1address.html#a5ce6c7d5f9ef6a934563431b6dd09402", null ],
    [ "street", "class_ship_database_1_1_data_1_1address.html#aa374ff848a6770c9d056d2f53a2d011c", null ],
    [ "street_number", "class_ship_database_1_1_data_1_1address.html#a078b336928cce146012c64f4f289f942", null ]
];
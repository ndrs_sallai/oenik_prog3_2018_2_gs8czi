var interface_ship_database_1_1_logic_1_1_i_logic =
[
    [ "CreateClub", "interface_ship_database_1_1_logic_1_1_i_logic.html#af0bf0582bb9f72585b27def940c7e588", null ],
    [ "CreateOwner", "interface_ship_database_1_1_logic_1_1_i_logic.html#a2300f6b19402995cb0de161ec1ae6846", null ],
    [ "CreateShip", "interface_ship_database_1_1_logic_1_1_i_logic.html#a3b90ebf5e857f56dc2c76a7282de8762", null ],
    [ "DeleteClub", "interface_ship_database_1_1_logic_1_1_i_logic.html#ab0af84bf9a5db2a805ef4018ce0c6d2a", null ],
    [ "DeleteOwner", "interface_ship_database_1_1_logic_1_1_i_logic.html#a447f0fefa6b951137711dd817026cd73", null ],
    [ "DeleteShip", "interface_ship_database_1_1_logic_1_1_i_logic.html#a173f4e37cfc09766f20aded7023a83fe", null ],
    [ "ListClubs", "interface_ship_database_1_1_logic_1_1_i_logic.html#a0e044c9256ba4687dc251043fe58dd84", null ],
    [ "ListOwners", "interface_ship_database_1_1_logic_1_1_i_logic.html#ac3814a198953eb373426a4ff30568e3c", null ],
    [ "ListShips", "interface_ship_database_1_1_logic_1_1_i_logic.html#a8662a99f7337c858b293ea2ce25d461a", null ],
    [ "UpdateClub", "interface_ship_database_1_1_logic_1_1_i_logic.html#a7a27536c43e0a0ed9b145dda4b39c1bc", null ],
    [ "UpdateOwner", "interface_ship_database_1_1_logic_1_1_i_logic.html#a5d487847676ea3cb82bcee379e79e324", null ],
    [ "UpdateShip", "interface_ship_database_1_1_logic_1_1_i_logic.html#ac31561797d91e4270d2129798dcc7f43", null ]
];
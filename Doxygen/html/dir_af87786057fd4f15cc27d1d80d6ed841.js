var dir_af87786057fd4f15cc27d1d80d6ed841 =
[
    [ "ShipDatabase.Data", "dir_b3b4c55948ff5373439cdbbeba79c68d.html", "dir_b3b4c55948ff5373439cdbbeba79c68d" ],
    [ "ShipDatabase.Logic", "dir_bc526c929bc8418f80d7c6ba86d94954.html", "dir_bc526c929bc8418f80d7c6ba86d94954" ],
    [ "ShipDatabase.Logic.Tests", "dir_ee0a21cc3da4743d5fe0080e00f4c585.html", "dir_ee0a21cc3da4743d5fe0080e00f4c585" ],
    [ "ShipDatabase.Program", "dir_2fca43f22356c73d8092be80120c8fc3.html", "dir_2fca43f22356c73d8092be80120c8fc3" ],
    [ "ShipDatabase.Repository", "dir_b924e8f93cbf9d913213e49099cbeb31.html", "dir_b924e8f93cbf9d913213e49099cbeb31" ],
    [ "ShipDatabase.Repository.Tests", "dir_6d947099d43c63064cf84023bb281110.html", "dir_6d947099d43c63064cf84023bb281110" ],
    [ "ShipDatabaseWebService", "dir_ac1f6d1d60f800f07e66f52a6574562f.html", "dir_ac1f6d1d60f800f07e66f52a6574562f" ]
];
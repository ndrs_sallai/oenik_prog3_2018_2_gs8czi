var class_ship_database_1_1_data_1_1owner =
[
    [ "owner", "class_ship_database_1_1_data_1_1owner.html#adb3e80702e87e835bb75c2c2d9778252", null ],
    [ "address", "class_ship_database_1_1_data_1_1owner.html#a1cee47371e0814b211fafcf5c2f28cc8", null ],
    [ "address_id", "class_ship_database_1_1_data_1_1owner.html#a75f9108948255a274fd31c94766e339a", null ],
    [ "date_of_birth", "class_ship_database_1_1_data_1_1owner.html#ae542c18d96561dbb7c6b73f869005cd3", null ],
    [ "memberships", "class_ship_database_1_1_data_1_1owner.html#ae3d1a0a17770a9e7f1a13e7d104579b6", null ],
    [ "name", "class_ship_database_1_1_data_1_1owner.html#af32412600bc055fcefab3e21582fc119", null ],
    [ "ships", "class_ship_database_1_1_data_1_1owner.html#abd1ead39295d917dce886a61ef72b22d", null ],
    [ "ssn", "class_ship_database_1_1_data_1_1owner.html#a5754cf2a93dafbb8baf164f704ec5697", null ]
];
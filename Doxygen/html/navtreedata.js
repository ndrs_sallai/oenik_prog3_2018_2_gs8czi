var NAVTREE =
[
  [ "ShipDatabase", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users__andr_xC3_xA1s_work_oenik_prog3_2018_2_gs8czi_packages__castle_8_core_84_83_81__c_h_a_n_g_e_l_o_g.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__c_1__users__andr_xC3_xA1s_work_oenik_prog3_2018_2_gs8czi_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html", null ],
    [ "Webprog féléves feladat 2018/őszi félév", "md__c_1__users__andr_xC3_xA1s_work_oenik_prog3_2018_2_gs8czi__r_e_a_d_m_e.html", null ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_business_logic_8cs_source.html",
"class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_data_table.html#aa157d05e323d80e4b8cc8d7016a4709b",
"class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1owner_table_adapter.html#af8c8970c6ed3f508a611814ad4dd49c4"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var class_ship_database_1_1_data_1_1ship =
[
    [ "bedspace", "class_ship_database_1_1_data_1_1ship.html#a228f31140815ce79eb53bb4c001d4329", null ],
    [ "construction_date", "class_ship_database_1_1_data_1_1ship.html#afecf83d275b8965c84f8114b63f12c19", null ],
    [ "homeport", "class_ship_database_1_1_data_1_1ship.html#a55a4503f83d5c585817c8208e823b8e0", null ],
    [ "homeport_name", "class_ship_database_1_1_data_1_1ship.html#a7b3884b00b10d78c71afcb408902c2a6", null ],
    [ "manufacturer", "class_ship_database_1_1_data_1_1ship.html#adfb5d5ff90cca2f6cea74f6174cb9576", null ],
    [ "manufacturer_name", "class_ship_database_1_1_data_1_1ship.html#a46b80d48d7bc7fc68820f9cd0a419877", null ],
    [ "name", "class_ship_database_1_1_data_1_1ship.html#a74ba5346a2f38fdb367d72a84dd7b32b", null ],
    [ "owner", "class_ship_database_1_1_data_1_1ship.html#aafab6d244adb9b08eff7b9c09583cccb", null ],
    [ "owner_ssn", "class_ship_database_1_1_data_1_1ship.html#a13ee8bf270572e4c74c563cef4faa5f5", null ]
];
var class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row =
[
    [ "IsbedspaceNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a285bbb29cd82094d112d628da9b7e032", null ],
    [ "Isconstruction_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a1c458735e097ab1a70b897adcc23464a", null ],
    [ "Ishomeport_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#aa78d86eeb14e87b32f2ba7f9193713a2", null ],
    [ "Ismanufacturer_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a5b0c94162c79f5dbda17a96b2d356da4", null ],
    [ "Isowner_ssnNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a8423af09f20d26bfeb6be5f538f3b53f", null ],
    [ "SetbedspaceNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a2891eb9eac9cad2abeea6fc402b4a9a6", null ],
    [ "Setconstruction_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a894702fd6a13e323db660fe7e4fc5925", null ],
    [ "Sethomeport_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#ad0f28913447235efaf23db2f4771cc46", null ],
    [ "Setmanufacturer_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a37601582582c1c09e293380e55171f46", null ],
    [ "Setowner_ssnNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a83b93251815fea48280d29fb9016e26e", null ],
    [ "bedspace", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a8340bf92894dbc49192bd075a9920676", null ],
    [ "construction_date", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#abb5dfbe10f959d0c9d5fb2f9712b11fa", null ],
    [ "homeport_name", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a866cd76ac1d07479b6ff79747f362655", null ],
    [ "homeportRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a9b54d0d7b3158d8871c6e8d2f6573892", null ],
    [ "manufacturer_name", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#acdca898cbc5b7eb34e74feb892a0282c", null ],
    [ "manufacturerRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a47686793bbbbab92ac485c993a9fd4f2", null ],
    [ "name", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a408d06375d761ebbc8b905b878b3ff5b", null ],
    [ "owner_ssn", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a3bcfcc5185b161da0e34672842ad2013", null ],
    [ "ownerRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html#a2407841f15099054aade8e361281f42c", null ]
];
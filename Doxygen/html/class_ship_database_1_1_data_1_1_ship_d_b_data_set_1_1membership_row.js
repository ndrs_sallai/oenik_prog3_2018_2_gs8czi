var class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row =
[
    [ "Isclub_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a421365a2ac4123d659c6e92d74e9b506", null ],
    [ "Isis_activeNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#af156b0a5948f8d094b42621cbf8a595a", null ],
    [ "Isjoin_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a6a2f0881d8d2c94d1d69b8e18ebe8f9b", null ],
    [ "Isleave_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#af2d7436603b0bb6e40995e66490072b3", null ],
    [ "Isowner_ssnNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a52905b2ce3727fef2bc45d8f70cf689b", null ],
    [ "IstitleNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a4ba0b325200bf73647ba7e02c8a364d7", null ],
    [ "Setclub_nameNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a5f2ccb0cf69e86b6795609da7af4b302", null ],
    [ "Setis_activeNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#aaa743ffc2c3cca94cc3152c12518e4dc", null ],
    [ "Setjoin_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a7023201fade8756ec5a847e04516b1b6", null ],
    [ "Setleave_dateNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#acfa8dcf900c9993249c8fab57cead3ae", null ],
    [ "Setowner_ssnNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a4aaae9daee205f884b5339e0476f0b97", null ],
    [ "SettitleNull", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#acc2fe505ad37847cd4a2ff89919590cd", null ],
    [ "club_name", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#ab5d0c1aa2e74405ce281333de090be20", null ],
    [ "clubRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a7df04a2fd735d3c63f734df17d76c535", null ],
    [ "id", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#aeac6257aa2f9ab931f260bf7e908ddd4", null ],
    [ "is_active", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a4724944d24254035387e693104251c1f", null ],
    [ "join_date", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#aee4f183ec600ab5575b188e30e6d6c4d", null ],
    [ "leave_date", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#a36b113f053865b23c470bebb30cf4fab", null ],
    [ "owner_ssn", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#aa314abf65a52341b65b2f96858c3fb7a", null ],
    [ "ownerRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#abaec7f3cd0648df4d1bccea2e601ea2f", null ],
    [ "title", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html#afbd63523d4742964c5805759c8d742ce", null ]
];
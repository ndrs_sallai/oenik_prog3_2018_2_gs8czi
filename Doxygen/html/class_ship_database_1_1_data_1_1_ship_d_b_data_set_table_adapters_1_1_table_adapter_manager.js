var class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager =
[
    [ "UpdateOrderOption", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a69467e0bc3cc7975607a2bda504e06af", [
      [ "InsertUpdateDelete", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a69467e0bc3cc7975607a2bda504e06afa27b77cb15d3da7ded0250d0001bc6755", null ],
      [ "UpdateInsertDelete", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a69467e0bc3cc7975607a2bda504e06afa894fcc001e51f673d3fb5f3096473dd8", null ]
    ] ],
    [ "MatchTableAdapterConnection", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#ad9f86e9140da22a09818ac6f2c7d07c7", null ],
    [ "SortSelfReferenceRows", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a3c4a1d85a35aba39cd6ca6abbef6b595", null ],
    [ "UpdateAll", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a5e98a10aa895272c9a156023247ac22f", null ],
    [ "addressTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a0248180b1b11ceefe74ffb01b6a0a800", null ],
    [ "BackupDataSetBeforeUpdate", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a0ddd52d06729820746cd6e2bacc28abf", null ],
    [ "clubTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a69a7df33b3d5e61ad677b2746bd755d3", null ],
    [ "Connection", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a038d2afc850ea945a1b31b32ffaad72b", null ],
    [ "homeportTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a4570d64c62c8bc9f48b4487850b0ed8b", null ],
    [ "manufacturerTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a26eab4e5df939a6c4ed9987a4a9bafc3", null ],
    [ "membershipTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a8f34b0a6e843a879593518a3cd615834", null ],
    [ "ownerTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#ada49e21bf0525112796fcb798c1a055e", null ],
    [ "shipTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#a865af5077e06daea930750bbff3a071d", null ],
    [ "TableAdapterInstanceCount", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#acf3a1ce01e70a277db56e1b8c8e19b3f", null ],
    [ "UpdateOrder", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html#ad6d71c1dcee055e31dceed7a2d0aa908", null ]
];
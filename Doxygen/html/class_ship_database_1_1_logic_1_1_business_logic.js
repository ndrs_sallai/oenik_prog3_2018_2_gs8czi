var class_ship_database_1_1_logic_1_1_business_logic =
[
    [ "BusinessLogic", "class_ship_database_1_1_logic_1_1_business_logic.html#a1a07448c1d5ceb165bed9e3612156e53", null ],
    [ "Add", "class_ship_database_1_1_logic_1_1_business_logic.html#ad378972e5a746067095b34c04c6a194d", null ],
    [ "CreateClub", "class_ship_database_1_1_logic_1_1_business_logic.html#a5a1cb4d6b0eaf969eb9e8a9d0da0ba58", null ],
    [ "CreateOwner", "class_ship_database_1_1_logic_1_1_business_logic.html#ad311c5cca787ff26a2c52a7c56f662bf", null ],
    [ "CreateShip", "class_ship_database_1_1_logic_1_1_business_logic.html#a77d01d98872c9865de3f23230214ed3b", null ],
    [ "DeleteClub", "class_ship_database_1_1_logic_1_1_business_logic.html#aac14af627bb15b2b72dfe2366da1b818", null ],
    [ "DeleteOwner", "class_ship_database_1_1_logic_1_1_business_logic.html#af9b754b64a3caa494c7982a08024a939", null ],
    [ "DeleteShip", "class_ship_database_1_1_logic_1_1_business_logic.html#a5f081de5e10e5b2e30b28169e7e0c114", null ],
    [ "ListClubs", "class_ship_database_1_1_logic_1_1_business_logic.html#afb65a8abc0a99dae9658c06fd12e094b", null ],
    [ "ListOwners", "class_ship_database_1_1_logic_1_1_business_logic.html#af66bf5c49ec40fd933d0708f16dc7806", null ],
    [ "ListShips", "class_ship_database_1_1_logic_1_1_business_logic.html#a680f911667f52647b909fe362d2226b6", null ],
    [ "UpdateClub", "class_ship_database_1_1_logic_1_1_business_logic.html#a52e8ffcde4da07f14a164f86814f2a4f", null ],
    [ "UpdateOwner", "class_ship_database_1_1_logic_1_1_business_logic.html#a1134247e1a9fd02d1631e7c37a0e356a", null ],
    [ "UpdateShip", "class_ship_database_1_1_logic_1_1_business_logic.html#acb4fff5deb6343b9f9a91c1e9bce581a", null ]
];
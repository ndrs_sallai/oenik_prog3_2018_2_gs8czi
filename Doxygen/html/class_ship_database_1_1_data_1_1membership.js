var class_ship_database_1_1_data_1_1membership =
[
    [ "club", "class_ship_database_1_1_data_1_1membership.html#a3748c62df475f7650fc5df21786fcc3a", null ],
    [ "club_name", "class_ship_database_1_1_data_1_1membership.html#a433612f005a8e40b5ca64fdee9e40727", null ],
    [ "id", "class_ship_database_1_1_data_1_1membership.html#a3578e474f0a085e7bcfbddedacb2255d", null ],
    [ "is_active", "class_ship_database_1_1_data_1_1membership.html#ac519ca2bee72ebe9d644271ac6da6139", null ],
    [ "join_date", "class_ship_database_1_1_data_1_1membership.html#ada3e316b0b1bde4b70c23e0f08c35fbc", null ],
    [ "leave_date", "class_ship_database_1_1_data_1_1membership.html#aaef467243a2d8fe0aa2e97a5875a26be", null ],
    [ "owner", "class_ship_database_1_1_data_1_1membership.html#a305f5f10a5952ee551c3d43b06f6823c", null ],
    [ "owner_ssn", "class_ship_database_1_1_data_1_1membership.html#a67ea0285abbb112e82304bc381fb3907", null ],
    [ "title", "class_ship_database_1_1_data_1_1membership.html#af5b531c3499eb9af2b9d5faa4285051b", null ]
];
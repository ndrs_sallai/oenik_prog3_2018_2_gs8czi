var hierarchy =
[
    [ "ShipDatabase.Data.address", "class_ship_database_1_1_data_1_1address.html", null ],
    [ "ShipDatabase.Data.club", "class_ship_database_1_1_data_1_1club.html", null ],
    [ "Component", null, [
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.addressTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1address_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.clubTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1club_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.homeportTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1homeport_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.manufacturerTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1manufacturer_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.membershipTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1membership_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.ownerTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1owner_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.shipTableAdapter", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1ship_table_adapter.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSetTableAdapters.TableAdapterManager", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_table_adapters_1_1_table_adapter_manager.html", null ]
    ] ],
    [ "DataRow", null, [
      [ "ShipDatabase.Data.ShipDBDataSet.addressRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1address_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.clubRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1club_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.homeportRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1homeport_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.manufacturerRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1manufacturer_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.membershipRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.ownerRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.shipRow", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row.html", null ]
    ] ],
    [ "DataSet", null, [
      [ "ShipDatabase.Data.ShipDBDataSet", "class_ship_database_1_1_data_1_1_ship_d_b_data_set.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "ShipDatabase.Data.ShipDBEntities", "class_ship_database_1_1_data_1_1_ship_d_b_entities.html", null ]
    ] ],
    [ "ShipDatabase.Data.homeport", "class_ship_database_1_1_data_1_1homeport.html", null ],
    [ "ShipDatabase.Logic.ILogic", "interface_ship_database_1_1_logic_1_1_i_logic.html", [
      [ "ShipDatabase.Logic.BusinessLogic", "class_ship_database_1_1_logic_1_1_business_logic.html", null ]
    ] ],
    [ "ShipDatabase.Repository.IRepository", "interface_ship_database_1_1_repository_1_1_i_repository.html", [
      [ "ShipDatabase.Repository.MyRepository", "class_ship_database_1_1_repository_1_1_my_repository.html", null ]
    ] ],
    [ "ShipDatabase.Item", "class_ship_database_1_1_item.html", null ],
    [ "ShipDatabase.ItemList", "class_ship_database_1_1_item_list.html", null ],
    [ "ShipDatabase.Logic.Tests.LogicTest", "class_ship_database_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "ShipDatabase.Data.manufacturer", "class_ship_database_1_1_data_1_1manufacturer.html", null ],
    [ "ShipDatabase.Data.membership", "class_ship_database_1_1_data_1_1membership.html", null ],
    [ "ShipDatabase.Program.MyProgram", "class_ship_database_1_1_program_1_1_my_program.html", null ],
    [ "ShipDatabase.Data.owner", "class_ship_database_1_1_data_1_1owner.html", null ],
    [ "ShipDatabase.Repository.Tests.RepositoryTest", "class_ship_database_1_1_repository_1_1_tests_1_1_repository_test.html", null ],
    [ "ShipDatabase.Data.ship", "class_ship_database_1_1_data_1_1ship.html", null ],
    [ "SystemEventArgs", null, [
      [ "ShipDatabase.Data.ShipDBDataSet.addressRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1address_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.clubRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1club_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.homeportRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1homeport_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.manufacturerRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1manufacturer_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.membershipRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.ownerRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_row_change_event.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.shipRowChangeEvent", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_row_change_event.html", null ]
    ] ],
    [ "TypedTableBase", null, [
      [ "ShipDatabase.Data.ShipDBDataSet.addressDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1address_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.clubDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1club_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.homeportDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1homeport_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.manufacturerDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1manufacturer_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.membershipDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1membership_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.ownerDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1owner_data_table.html", null ],
      [ "ShipDatabase.Data.ShipDBDataSet.shipDataTable", "class_ship_database_1_1_data_1_1_ship_d_b_data_set_1_1ship_data_table.html", null ]
    ] ],
    [ "HttpServlet", null, [
      [ "ShipDatabase.OfferServlet", "class_ship_database_1_1_offer_servlet.html", null ]
    ] ]
];
var interface_ship_database_1_1_repository_1_1_i_repository =
[
    [ "CreateClub", "interface_ship_database_1_1_repository_1_1_i_repository.html#ad9fa65e3d8019fac2cc903a945bd5b27", null ],
    [ "CreateOwner", "interface_ship_database_1_1_repository_1_1_i_repository.html#ae804c8d8ee6b0fb595d04e10470bf007", null ],
    [ "CreateShip", "interface_ship_database_1_1_repository_1_1_i_repository.html#a11840a66573c937f20a1a95ac1cd56e6", null ],
    [ "DeleteClub", "interface_ship_database_1_1_repository_1_1_i_repository.html#a09400678411b3fe7a9f7c9fbe1090b48", null ],
    [ "DeleteOwner", "interface_ship_database_1_1_repository_1_1_i_repository.html#a1169a58bde07b372519ab3228a5216c4", null ],
    [ "DeleteShip", "interface_ship_database_1_1_repository_1_1_i_repository.html#affe50a35516d60a95abb124f70a570ad", null ],
    [ "ListClubs", "interface_ship_database_1_1_repository_1_1_i_repository.html#a9d891108c748efc4d7d57dbf9a011c8c", null ],
    [ "ListOwners", "interface_ship_database_1_1_repository_1_1_i_repository.html#ab9cef3887a8a71d3676b7e8559318850", null ],
    [ "ListShips", "interface_ship_database_1_1_repository_1_1_i_repository.html#afac661fb0bdb5620c29a1e7cf795fa58", null ],
    [ "UpdateClub", "interface_ship_database_1_1_repository_1_1_i_repository.html#a802a83109047c00c746c13c83f4b15aa", null ],
    [ "UpdateOwner", "interface_ship_database_1_1_repository_1_1_i_repository.html#a4340a0552ab2b5026aa8e822f5ebdbfc", null ],
    [ "UpdateShip", "interface_ship_database_1_1_repository_1_1_i_repository.html#a7b8578245e78ca9a96e18421683b13f2", null ]
];
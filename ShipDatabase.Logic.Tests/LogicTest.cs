﻿//-----------------------------------------------------------------------
// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Logic.Tests
{
    using Moq;
    using NUnit.Framework;

    using ShipDatabase.Repository;

    /// <summary>
    /// LogicTest class
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// Dummy test
        /// </summary>
        [Test]
        public void LogicAddShouldAddTwoIntegers()
        {
            // Arrange
            IRepository repo = new MyRepository();
            BusinessLogic l = new BusinessLogic(repo);

            // Act
            int result = l.Add(3, 2);

            // Assert
            Assert.That(result, Is.EqualTo(5));
        }

        /// <summary>
        /// Test create ship
        /// </summary>
        [Test]
        public void TestCreateShipCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.CreateShip("asd", 3, "1234");
            repo.Verify(m => m.CreateShip("asd", 3, "1234"), Times.Once);
        }

        /// <summary>
        /// Test create owner
        /// </summary>
        [Test]
        public void TestCreateOwnerCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.CreateOwner("333444555", "Matyas Kiraly");
            repo.Verify(m => m.CreateOwner("333444555", "Matyas Kiraly"), Times.Once);
        }

        /// <summary>
        /// Test create club
        /// </summary>
        [Test]
        public void TestCreateClubCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.CreateClub("Fekete Vitorlák", "123456789");
            repo.Verify(m => m.CreateClub("Fekete Vitorlák", "123456789"), Times.Once);
        }

        /// <summary>
        /// Test list ship
        /// </summary>
        [Test]
        public void TestListShipsCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.ListShips();
            repo.Verify(m => m.ListShips(), Times.Once);
        }

        /// <summary>
        /// Test list owners
        /// </summary>
        [Test]
        public void TestListOwnerssCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.ListOwners();
            repo.Verify(m => m.ListOwners(), Times.Once);
        }

        /// <summary>
        /// Test list clubs
        /// </summary>
        [Test]
        public void TestListClubssCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.ListClubs();
            repo.Verify(m => m.ListClubs(), Times.Once);
        }

        /// <summary>
        /// Test delete ship
        /// </summary>
        [Test]
        public void TestDeleteShipCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.DeleteShip("asdf");
            repo.Verify(m => m.DeleteShip("asdf"), Times.Once);
        }

        /// <summary>
        /// Test delete owner
        /// </summary>
        [Test]
        public void TestDeleteOwnerCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.DeleteOwner("123456789");
            repo.Verify(m => m.DeleteOwner("123456789"), Times.Once);
        }

        /// <summary>
        /// Test delete club
        /// </summary>
        [Test]
        public void TestDeleteClubCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.DeleteClub("123456789");
            repo.Verify(m => m.DeleteClub("123456789"), Times.Once);
        }

        /// <summary>
        /// Test update ship
        /// </summary>
        [Test]
        public void TestUpdateShipCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.UpdateShip();
            repo.Verify(m => m.UpdateShip(), Times.Once);
        }

        /// <summary>
        /// Test update owner
        /// </summary>
        [Test]
        public void TestUpdateOwnerCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.UpdateOwner();
            repo.Verify(m => m.UpdateOwner(), Times.Once);
        }

        /// <summary>
        /// Test update club
        /// </summary>
        [Test]
        public void TestUpdateClubCallsRepository()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            l.UpdateClub();
            repo.Verify(m => m.UpdateClub(), Times.Once);
        }

        /// <summary>
        /// Non-CRUD method
        /// returns a string representation of the given ship
        /// </summary>
        [Test]
        public void PresentShip()
        {
            Mock<IRepository> repo = new Mock<IRepository>();
            BusinessLogic l = new BusinessLogic(repo.Object);

            Data.ship s = new Data.ship()
            {
                name = "Ivan",
                homeport_name = "Marseille",
                manufacturer_name = "Lockheed",
                owner_ssn = "123"
            };

            Assert.That(l.PresentShip(s), Is.EqualTo("Ivan (Lockheed) stationed in Marseille"));
        }
    }
}

﻿//-----------------------------------------------------------------------
// <copyright file="MyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Repository
{
    using System.Linq;

    using ShipDatabase.Data;

    /// <summary>
    /// The ship repository class
    /// </summary>
    public class MyRepository : IRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyRepository"/> class.
        /// initializes Context
        /// </summary>
        public MyRepository()
        {
            this.Context = new ShipDBEntities();
        }

        /// <summary>
        /// Gets Context
        /// </summary>
        public ShipDBEntities Context { get; private set; }

        /// <summary>
        /// List ships
        /// </summary>
        /// <returns>ship list</returns>
        public IQueryable<ship> ListShips()
        {
            return from s in this.Context.ships select s;
        }

        /// <summary>
        /// List owners
        /// </summary>
        /// <returns>owner list</returns>
        public IQueryable<owner> ListOwners()
        {
            return from o in this.Context.owners select o;
        }

        /// <summary>
        /// List clubs
        /// </summary>
        /// <returns>club list</returns>
        public IQueryable<club> ListClubs()
        {
            return from c in this.Context.clubs select c;
        }

        /// <summary>
        /// Create ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="bedspace">bedspace</param>
        /// <param name="ownerSsn">ownerSsn</param>
        /// <returns>ship entity</returns>
        public ship CreateShip(string name, short? bedspace, string ownerSsn)
        {
            var newShip = new ship()
            {
                name = name,
                bedspace = bedspace,
                owner_ssn = ownerSsn
            };

            this.Context.ships.Add(newShip);

            this.SaveChanges();

            return newShip;
        }

        /// <summary>
        /// Create owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        /// <param name="name">name</param>
        /// <returns>owner entity</returns>
        public owner CreateOwner(string ssn, string name)
        {
            var newOwner = new owner()
            {
                name = name,
                ssn = ssn
            };

            this.Context.owners.Add(newOwner);

            this.SaveChanges();

            return newOwner;
        }

        /// <summary>
        /// Create club
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <returns>club entity</returns>
        public club CreateClub(string name, string phoneNumber)
        {
            var newClub = new club()
            {
                name = name,
                phone_number = phoneNumber
            };

            this.Context.clubs.Add(newClub);

            this.SaveChanges();

            return newClub;
        }

        /// <summary>
        /// Update ship
        /// </summary>
        public void UpdateShip()
        {
            this.SaveChanges();
        }

        /// <summary>
        /// Update owner
        /// </summary>
        public void UpdateOwner()
        {
            this.SaveChanges();
        }

        /// <summary>
        /// Update club
        /// </summary>
        public void UpdateClub()
        {
            this.SaveChanges();
        }

        /// <summary>
        /// Delete ship by name
        /// </summary>
        /// <param name="name">name of the ship</param>
        public void DeleteShip(string name)
        {
            try
            {
                var ship = this.Context.ships.Single(x => x.name == name);

                this.Context.ships.Remove(ship);
                this.SaveChanges();
            }
            catch (System.InvalidOperationException e)
            {
                System.Console.WriteLine("Can't delete: " + e.Message);
            }
        }

        /// <summary>
        /// Delete owner by ssn
        /// </summary>
        /// <param name="ssn">ssn of owner</param>
        public void DeleteOwner(string ssn)
        {
            try
            {
                var owner = this.Context.owners.Single(x => x.ssn == ssn);

                this.Context.owners.Remove(owner);
                this.SaveChanges();
            }
            catch (System.InvalidOperationException e)
            {
                System.Console.WriteLine("Can't delete: " + e.Message);
            }
        }

        /// <summary>
        /// Delete club by name
        /// </summary>
        /// <param name="name">name of club</param>
        public void DeleteClub(string name)
        {
            try
            {
                var club = this.Context.clubs.Single(x => x.name == name);

                this.Context.clubs.Remove(club);
                this.SaveChanges();
            }
            catch (System.InvalidOperationException e)
            {
                System.Console.WriteLine("Can't delete: " + e.Message);
            }
        }

        /// <summary>
        /// Calls SaveChanges() on db context object.
        /// </summary>
        private void SaveChanges()
        {
            try
            {
                this.Context.SaveChanges();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException e)
            {
                System.Console.WriteLine("Can't update context: " + e.InnerException.InnerException.Message);
            }
        }
    }
}

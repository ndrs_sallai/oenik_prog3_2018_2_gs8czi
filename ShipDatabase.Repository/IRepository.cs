﻿//-----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Repository
{
    using System.Linq;
    using ShipDatabase.Data;

    /// <summary>
    /// IRepository interface
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Create ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="bedspace">bedspace</param>
        /// <param name="ownerSsn">owner ssn</param>
        /// <returns>the ship entity</returns>
        ship CreateShip(string name, short? bedspace, string ownerSsn);

        /// <summary>
        /// Lists ships
        /// </summary>
        /// <returns>ship list</returns>
        IQueryable<ship> ListShips();

        /// <summary>
        /// Update ship
        /// </summary>
        void UpdateShip();

        /// <summary>
        /// Delete ship
        /// </summary>
        /// <param name="name">name</param>
        void DeleteShip(string name);

        /// <summary>
        /// Create owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        /// <param name="name">name</param>
        /// <returns>the owner entity</returns>
        owner CreateOwner(string ssn, string name);

        /// <summary>
        /// List owners
        /// </summary>
        /// <returns>the owner list</returns>
        IQueryable<owner> ListOwners();

        /// <summary>
        /// Update Owner
        /// </summary>
        void UpdateOwner();

        /// <summary>
        /// Delete owner
        /// </summary>
        /// <param name="ssn">ssn</param>
        void DeleteOwner(string ssn);

        /// <summary>
        /// Create club
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="phoneNumber">phone</param>
        /// <returns>te club entity</returns>
        club CreateClub(string name, string phoneNumber);

        /// <summary>
        /// List club
        /// </summary>
        /// <returns>the club list</returns>
        IQueryable<club> ListClubs();

        /// <summary>
        /// Update club
        /// </summary>
        void UpdateClub();

        /// <summary>
        /// Delete club
        /// </summary>
        /// <param name="name">name</param>
        void DeleteClub(string name);
    }
}

﻿//-----------------------------------------------------------------------
// <copyright file="MyProgram.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace ShipDatabase.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Main program
    /// </summary>
    public sealed class MyProgram
    {
        private MyProgram()
        {
        }

        /// <summary>
        /// Main method
        /// </summary>
        public static void Main()
        {
            ShowMenu();
        }

        /// <summary>
        /// Sets up main menu items, calls menu.Display() in an endless loop.
        /// </summary>
        private static void ShowMenu()
        {
            var menu = new EasyConsole.Menu()
                .Add("Ships", MyProgram.ShipSubMenu)
                .Add("Owners", MyProgram.OwnerSubMenu)
                .Add("Clubs", MyProgram.ClubSubMenu)
                .Add("Get Prices", () => { MyProgram.ShowEntityList(MyProgram.GetPrices()); });

            while (true)
            {
                menu.Display();
                Console.WriteLine("\n");
            }
        }

        /// <summary>
        /// Shows ship submenu
        /// </summary>
        private static void ShipSubMenu()
        {
            Repository.IRepository repo = new Repository.MyRepository();
            Logic.ILogic logic = new Logic.BusinessLogic(repo);

            var ships = logic.ListShips();

            var subMenu = new EasyConsole.Menu()
                .Add("Create ship", () => { logic.CreateShip("Malac", 3, "999888777"); })
                .Add("List ships", () => { MyProgram.ShowEntityList(ships); })
                .Add("Update ship", () =>
                {
                    var ship = ships.First();
                    ship.bedspace = 4;
                    logic.UpdateShip();
                })
                .Add("Delete ship", () =>
                {
                    logic.DeleteShip("Maverick III.");
                });

            subMenu.Display();
        }

        /// <summary>
        /// Shows owner submenu
        /// </summary>
        private static void OwnerSubMenu()
        {
            Repository.IRepository repo = new Repository.MyRepository();
            Logic.ILogic logic = new Logic.BusinessLogic(repo);

            var owners = logic.ListOwners();

            var subMenu = new EasyConsole.Menu()
                .Add("Create owner", () => { logic.CreateOwner("31323131", "The Red Baron"); })
                .Add("List owners", () => { MyProgram.ShowEntityList(logic.ListOwners()); })
                .Add("Update owner", () =>
                {
                    var owner = owners.First();
                    owner.name = "Elek Vilmos";
                    logic.UpdateOwner();
                })
                .Add("Delete owner", () =>
                {
                    logic.DeleteOwner("135797531");
                });

            subMenu.Display();
        }

        /// <summary>
        /// Shows club submenu
        /// </summary>
        private static void ClubSubMenu()
        {
            Repository.IRepository repo = new Repository.MyRepository();
            Logic.ILogic logic = new Logic.BusinessLogic(repo);

            var clubs = logic.ListOwners();

            var subMenu = new EasyConsole.Menu()
                .Add("Create club", () => { logic.CreateClub("Pirate bay", "0124524612"); })
                .Add("List clubs", () => { MyProgram.ShowEntityList(logic.ListClubs()); })
                .Add("Update club", () =>
                {
                    var club = clubs.First();
                    club.name = "Sirály Kloákája";
                    logic.UpdateClub();
                })
                .Add("Delete club", () =>
                {
                    logic.DeleteClub("A legkedveltebb klub");
                });

            subMenu.Display();
        }

        /// <summary>
        /// Calls the java-based web service endpoint.
        /// </summary>
        /// <returns>items</returns>
        private static IEnumerable<object> GetPrices()
        {
            // hard coded values for now...
            string host = "192.168.0.17";
            int port = 8084;

            XDocument response = XDocument.Load("http://" + host + ":" + port + "/Offer?customer=J.S.Bach&model=starship&price=200");

            var items = from i in response.Descendants("item")
            select new
            {
                Model = i.Element("model"),
                Price = i.Element("price"),
                Customer = i.Element("customer")
            };

            return items;
        }

        /// <summary>
        /// Prints out the given list using reflection
        /// </summary>
        /// <param name="list">list of entites</param>
        private static void ShowEntityList(IEnumerable<object> list)
        {
            foreach (var item in list)
            {
                var elements =
                    from property in item.GetType().GetProperties()
                    select property.Name + ": " + property.GetValue(item, null);

                Console.WriteLine(string.Join(", ", elements.ToArray()));
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShipDatabase.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class membership
    {
        public long id { get; set; }
        public string owner_ssn { get; set; }
        public string club_name { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> join_date { get; set; }
        public Nullable<System.DateTime> leave_date { get; set; }
        public Nullable<short> is_active { get; set; }
    
        public virtual club club { get; set; }
        public virtual owner owner { get; set; }
    }
}

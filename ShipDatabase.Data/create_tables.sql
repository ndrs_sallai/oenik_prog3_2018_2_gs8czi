﻿create table address (
    id bigint not null,
    postal_code smallint,
    city varchar(50),
    street varchar(50),
    street_number varchar(10)
    constraint address_pk primary key(id)
);

create table manufacturer (
    name varchar(50) not null,
    address_id bigint,
    constraint manufacturer_pk primary key(name),
    constraint manufacturer_address_fk foreign key(address_id) references address(id)
);

create table homeport (
    name varchar(50) not null,
    address_id bigint,
    phone_number varchar(12),
    capacity smallint ,
    constraint homeport_pk primary key(name),
    constraint homeport_address_fk foreign key(address_id) references address(id)
);

create table owner (
    ssn varchar(9) not null,
    name varchar(50) not null,
    date_of_birth datetime,
    address_id bigint,
    constraint owner_pk primary key(ssn),
    constraint owner_address_fk foreign key(address_id) references address(id)
);

create table club (
    name varchar(50) not null,
    address_id bigint,
    phone_number varchar(12),
    constraint club_pk primary key(name),
    constraint club_address_fk foreign key(address_id) references address(id)
);

create table membership (
    id bigint not null,
    owner_ssn varchar(9),
    club_name varchar(50),
    title varchar(50),
    join_date datetime,
    leave_date datetime,
    is_active smallint,
    constraint membership_pk primary key(id),
    constraint member_ssn_fk foreign key(owner_ssn) references owner(ssn),
    constraint member_club_fk foreign key(club_name) references club(name),
    constraint date_check check(join_date < leave_date)
);

create table ship (
    name varchar(50) not null,
    construction_date datetime,
    bedspace smallint,
    homeport_name varchar(50),
    manufacturer_name varchar(50),
    owner_ssn varchar(9),
    constraint ship_pk primary key(name),
    constraint ship_homeport_fk foreign key(homeport_name) references homeport(name),
    constraint ship_manufacturer_fk foreign key(manufacturer_name) references manufacturer(name),
    constraint ship_owner_fk foreign key(owner_ssn) references owner(ssn)
);

﻿-- address
insert into address values (
    1, 3035, 'Badacsonytomaj', 'Aladár utca', '3/a'
)

insert into address values (
    2, 2233, 'Dunaújváros', 'Kossuth utca', '1'
)

insert into address values (
    3, 4138, 'Balatonszárszó', 'Jókai utca', '12'
)

insert into address values (
    4, 5733, 'Szeged', 'Széchenyi', '82'
)

insert into address values (
    5, 7100, 'Szigliget', 'Almás sor', '5'
)

insert into address values (
    6, 4321, 'Balatonfüred', 'Fenyő sor', '10'
)

-- manufacturer
insert into manufacturer values (
    'Skunk Works', 1
)

insert into manufacturer values (
    'Oracle', 2
)

insert into manufacturer values (
    'Charles Ernest Nicholson', 3
)

-- homeport
insert into homeport values (
    'Nádfedeles I.', 4, '01234567', 12
)

insert into homeport values (
    'Fecske Fészek', 2, '5544332211', 8
)

insert into homeport values (
    'Vasmacska III.', 3, '987654321', 20
)

-- owner
insert into owner values (
    '123456789', 'Wolfgang A. Mozart', convert(DATETIME, '1885.01.02', 102), 5
)

insert into owner values (
    '140500600', 'J.S. Bach', convert(DATETIME, '1845.10.08', 102), 2
)

insert into owner values (
    '999888777', 'Nils Holgersson', convert(DATETIME, '1940.03.02', 102), 1
)

insert into owner values (
    '135797531', 'Shipless Nick', convert(DATETIME, '1800.01.01', 102), 6
)

-- club
insert into club values (
    'Szárnyalj Fennen', 3, '01234567'
)

insert into club values (
    'Sissy Yacht Club', 4, '01234567'
)

insert into club values (
    'Szakadt vitorla', 2, '01234567'
)

insert into club values (
    'A legkedveltebb klub', 1, '54981201'
)

-- membership
insert into membership values (
    1, '123456789', 'Szárnyalj Fennen', 'officer', convert(DATETIME, '2008.12.05', 102), null, 1
)

insert into membership values (
    2, '140500600', 'Sissy Yacht Club', 'rear composer', convert(DATETIME, '2004.12.05', 102), convert(DATETIME, '2011.03.04', 102), 0
)

insert into membership values (
    3, '999888777', 'Szakadt vitorla', 'first officer', convert(DATETIME, '2010.06.12', 102), null, 1
)

-- ship
insert into ship values (
    'Pepe', convert(DATETIME, '2007.01.01', 102), 6, 'Nádfedeles I.', 'Skunk Works', '140500600'
)

insert into ship values (
    'Felhő', convert(DATETIME, '2004.01.01', 102), 8, 'Vasmacska III.', 'Oracle', '999888777'
)

insert into ship values (
    'Titan IV.', convert(DATETIME, '2001.02.01', 102), 8, 'Fecske Fészek', 'Charles Ernest Nicholson', '123456789'
)

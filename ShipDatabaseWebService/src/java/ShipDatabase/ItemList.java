package ShipDatabase;

import java.util.ArrayList;
import java.util.List;

public class ItemList {
    private List<Item> list;

    public ItemList() {
        this.list = new ArrayList<Item>();
    }
    
    public void add(Item i) {
        this.list.add(i);
    }
    
}

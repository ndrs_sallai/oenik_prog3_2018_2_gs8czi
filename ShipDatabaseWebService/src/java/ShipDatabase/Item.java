package ShipDatabase;

public class Item {
    private String model;
    private int price;
    private String customer;

    public Item(String model, int price, String customer) {
        this.model = model;
        this.price = price;
        this.customer = customer;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public String getCustomer() {
        return customer;
    }
}
